<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

$arComponentDescription = array(
    'NAME' => GetMessage("NAN_FAVOURITE_PAGE"),
    "DESCRIPTION" => GetMessage("NAN_FAVOURITE_PAGE_DESCRIPTION"),
    "ICON" => "/images/sections_top_count.gif",
    "CACHE_PATH" => "Y",
    'PATH' => array(
        'ID' => 'nan',
        'NAME' => 'NaN',
    ),
);
