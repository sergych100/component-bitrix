<?php

use Bitrix\Iblock;
use Bitrix\Main\Application;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;

/** @var array $arCurrentValues */
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
$context = Application::getInstance()->getContext();

if (!Loader::includeModule('iblock')) {
    return;
}
    $iblockTypes = array();
    $iblockTypeList = Iblock\TypeTable::getList(array(
        'select' => array('ID', 'NAME' => 'LANG_MESSAGE.NAME'),
        'filter' => array('=LANG_MESSAGE.LANGUAGE_ID' => $context->getLanguage()),
        'order' => array('LANG_MESSAGE.NAME' => 'ASC'),
    ));
    while ($iblockType = $iblockTypeList->fetch()) {
        $iblockTypes[$iblockType['ID']] = sprintf('[%s] %s', $iblockType['ID'], $iblockType['NAME']);
    }
    unset($iblockType, $iblockTypeList);

    $iblocks = array();
    $iblockFilter = array('=ACTIVE' => 'Y');
    if (!empty($arCurrentValues['IBLOCK_TYPE'])) {
        $iblockFilter['=IBLOCK_TYPE_ID'] = (string)$arCurrentValues['IBLOCK_TYPE'];
    }
    $iblockList = Iblock\IblockTable::getList(array(
        'select' => array('ID', 'NAME'),
        'filter' => $iblockFilter,
        'order' => array('NAME' => 'ASC'),
    ));
    while ($iblock = $iblockList->fetch()) {
        $iblocks[$iblock['ID']] = sprintf('[%d] %s', $iblock['ID'], $iblock['NAME']);
    }
    unset($iblock, $iblockList, $iblockFilter);


$arComponentParameters = array(
    'GROUPS' => array(),
    'PARAMETERS' => array(
        'IBLOCK_TYPE' => array(
            'PARENT' => 'BASE',
            'NAME' => GetMessage("NAN_IBLOCK_TYPE"),
            'TYPE' => 'LIST',
            'ADDITIONAL_VALUES' => 'Y',
            'VALUES' => $iblockTypes,
            'REFRESH' => 'Y',
        ),
        'IBLOCK_ID' => array(
            'PARENT' => 'BASE',
            'NAME' => GetMessage("NAN_IBLOCK_ID"),
            'TYPE' => 'LIST',
            'ADDITIONAL_VALUES' => 'Y',
            'VALUES' => $iblocks,
            'REFRESH' => 'Y',
        ),
        "USER_AUTHORIZED" => array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("NAN_USER_AUTHORIZED"),
            "TYPE" => "CHECKBOX",
            "DEFAULT" => "N",
        ),
        "USER_PROP_FAVOURITE" => array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("NAN_USER_PROP_FAVOURITE"),
            "TYPE" => "STRING",
            "DEFAULT" => "",
        ),
        "CACHE_TIME"  =>  Array("DEFAULT"=>36000000),

    ),
);