<?
namespace NaN\Components;

use Bitrix\Main\Loader,
	Bitrix\Main\Type;
use \Bitrix\Main\Web\Json;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Application;
use Bitrix\Main\Engine\ActionFilter;
use Bitrix\Main\Web\Cookie;
use Bitrix\Main\DI\ServiceLocator;
use Bitrix\Main\Engine\Contract\Controllerable;
use Bitrix\Main\Error;
use Bitrix\Main\ErrorCollection;


Loader::includeModule('iblock');
Loc::loadMessages(__FILE__);

class FavouriteComponent extends \CBitrixComponent implements Controllerable, \Bitrix\Main\Errorable
{

    private $inCookies = false;
    private $isAuthed = false;
    private $userID;
    protected $errorCollection;

 
    

    public function manageFavouriteAction($id,$param)
    {

        global $USER;

            $user = new \CUser;
            if(!isset($param["USER_PROP_FAVOURITE"]) || strlen($this->arParams["USER_PROP_FAVOURITE"]) == 0){
                $this->arParams["USER_PROP_FAVOURITE"]  = $param["USER_PROP_FAVOURITE"];
            }
            $arFilter = array("ID" => $USER->GetID());
            $arParams["SELECT"] = array($this->arParams["USER_PROP_FAVOURITE"]);
            $arRes = \CUser::GetList(($by="ID"), ($order="ASC"),$arFilter,$arParams);
            $res = $arRes->Fetch();
            $added = false;
            $removed = false;

            if($res[$this->arParams["USER_PROP_FAVOURITE"]]) {
                $key = array_search($id,$res[$this->arParams["USER_PROP_FAVOURITE"]]);
                if(is_numeric($key) ){
                    unset($res[$this->arParams["USER_PROP_FAVOURITE"]][$key]);
                    $removed = true;
                }
                else {
                    $res[$this->arParams["USER_PROP_FAVOURITE"]][] = (int)$id;
                    $added = true;
                }
            }
            else {
                $res[$this->arParams["USER_PROP_FAVOURITE"]][] =   (int)$id;
                $added = true;
            }
            $fields = array(
                $this->arParams["USER_PROP_FAVOURITE"] => $res[$this->arParams["USER_PROP_FAVOURITE"]],
            );
            $user->Update($USER->GetID(), $fields);

        $result = [
            'success' => true,
            'favourites' =>$res[$this->arParams["USER_PROP_FAVOURITE"]],
            'status' => ['added'=>$added,'removed'=>$removed],
            'error' => ''
        ];


        return $result;
    }

    public function getFavouriteListIDs(){
        global $USER;
        $result = [];
        $cookieFav = [];
        $userFav = [];


            $request = Application::getInstance()->getContext()->getRequest();

            if(strlen($request->getCookie("FAVOURITE"))>0){
                $cookieFavJSON = $request->getCookie("FAVOURITE");
                $cookieFav = \Bitrix\Main\Web\Json::decode($cookieFavJSON);
            }


        if(!isset($this->arParams["USER_PROP_FAVOURITE"])){
            $this->arParams["USER_PROP_FAVOURITE"] = "UF_FAVOURITE";
        }
            $arFilter = array("ID" => $USER->GetID());
            $arParams["SELECT"] = [$this->arParams["USER_PROP_FAVOURITE"]];
            $arRes = \CUser::GetList(($by="ID"), ($order="ASC"),$arFilter,$arParams);
            $res = $arRes->Fetch();
            $userFav = $res[$this->arParams["USER_PROP_FAVOURITE"]];

            $arrayDiff = array_diff($cookieFav,$userFav);
            if($arrayDiff){
                $user = new \CUser;
                $fields = array(
                    $this->arParams["USER_PROP_FAVOURITE"] => array_merge($res[$this->arParams["USER_PROP_FAVOURITE"]],$arrayDiff)
                );
                $user->Update($USER->GetID(), $fields);
            }

        if($cookieFav){
            $cookie = new Cookie("FAVOURITE", '');
            $cookie->setDomain(SITE_SERVER_NAME);
            Application::getInstance()->getContext()->getResponse()->addCookie($cookie);
        }
        $result = array_unique(array_merge($userFav,$cookieFav));

        return $result;
    }

    public function getList(){
        $IDs = $this->getFavouriteListIDs();
        $fav = [];
        if(!empty($IDs)){
            $fav = \Bitrix\IBlock\ElementTable::getList([
                'select' =>["ID"],
                'filter' =>[
                    'IBLOCK_ID'=>$this->arParams["IBLOCK_ID"],
                    'ID' =>$IDs
                ],
            ])->fetchAll();
        }
        return $fav;
    }


    public function onPrepareComponentParams($arParams = array())
	{
        $this->errorCollection = new ErrorCollection();

        global $USER;
        if(!isset($this->arParams["USER_PROP_FAVOURITE"])){
            $this->arParams["USER_PROP_FAVOURITE"] = "UF_FAVOURITE";
        }
        //без авторизации используем куки
        $this->inCookies = !($this->arParams["USER_AUTHORIZED"] && $this->arParams["USER_AUTHORIZED"] == "Y");
        $this->isAuthed = $USER->IsAuthorized();
        $this->userID = $USER->GetID();
		return $arParams;
	}


	public function executeComponent()
	{
        if($this->inCookies && !$this->isAuthed){
            ShowError(GetMessage("NAN_NEED_AUTH"));
            return;
            //запрет неавторизованным пользователям
        }


        $this->arResult["ELEMENT_ID"] =  $this->getFavouriteListIDs();

        $this->includeComponentTemplate();

	}


    public function configureActions(): array
    {
        return [
            'addFavourite' => [
                'prefilters' => [
                    new ActionFilter\HttpMethod(
                        [ActionFilter\HttpMethod::METHOD_POST]
                    ),
                    new ActionFilter\Authentication(true),
                    new ActionFilter\Csrf(),
                ],
                'postfilters' => []
            ],
            'removeFavourite' => [
                'prefilters' => [
                    new ActionFilter\HttpMethod(
                        [ActionFilter\HttpMethod::METHOD_POST]
                    ),
                    new ActionFilter\Authentication(true),
                    new ActionFilter\Csrf(),
                ],
                'postfilters' => []
            ],
            'getFavourite' => [
                'prefilters' => [
                    new ActionFilter\HttpMethod(
                        [ActionFilter\HttpMethod::METHOD_POST]
                    ),
                    new ActionFilter\Authentication(true),
                    new ActionFilter\Csrf(),
                ],
                'postfilters' => []
            ],
            'manageFavourite' => [
                'prefilters' => [
                    new ActionFilter\HttpMethod(
                        [ActionFilter\HttpMethod::METHOD_POST]
                    ),
                    new ActionFilter\Authentication(true),
                    new ActionFilter\Csrf(),
                ],
                'postfilters' => [
                    ]
            ]
        ];
    }

    /**
     * Getting array of errors.
     * @return Error[]
     */
    public function getErrors()
    {
        return $this->errorCollection->toArray();
    }
    /**
     * Getting once error with the necessary code.
     * @param string $code Code of error.
     * @return Error
     */
    public function getErrorByCode($code)
    {
        return $this->errorCollection->getErrorByCode($code);
    }
}